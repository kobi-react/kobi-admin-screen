import { useCallback, useReducer } from "react";

const initialState = {
    loading: false,
    error: null,
    data: null,
    extra: null,
    identifier: null
};

const httpReducer = (curHttpState: any, action: any) => {
    switch (action.type) {
      case 'SEND':
        return {
          loading: true,
          error: null,
          data: null,
          extra: null,
          identifier: action.identifier
        };
      case 'RESPONSE':
        return {
          ...curHttpState,
          loading: false,
          data: action.responseData,
          extra: action.extra
        };
      case 'ERROR':
        return { loading: false, error: action.errorMessage };
      case 'CLEAR':
        return initialState;
      default:
        throw new Error('Should not be reached!');
    }
};

const useHttp = () => {
    const [httpState, dispatchHttp] = useReducer(httpReducer, initialState);
    
    // method 1
    const clear = useCallback(() => dispatchHttp({ type: 'CLEAR' }), []);

    // method 2
    const sendRequest = useCallback(
        (url, method, body?, reqExtra?, reqIdentifer?) => {
            dispatchHttp({ type: 'SEND', identifier: reqIdentifer });
            fetch(url, {
                method: method,
                body: body
            }).then(response => {
                return response.json();
            }).then(responseData => {
                dispatchHttp({
                  type: 'RESPONSE',
                  responseData: responseData,
                  extra: reqExtra
                });
            }).catch(error => {
                dispatchHttp({
                  type: 'ERROR',
                  errorMessage: error
                });
            });
        }, []
    );

    // return access to all mehod and properties
    return {
        isLoading: httpState.loading,
        data: httpState.data,
        error: httpState.error,
        sendRequest: sendRequest,
        reqExtra: httpState.extra,
        reqIdentifer: httpState.identifier,
        clear: clear
    };
};

export default useHttp;
