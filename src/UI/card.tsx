import React from 'react';

import styles from './card.module.scss';

const Card = (props: any) => {
  return <div className={styles.card} style={props.style}>{props.children}</div>;
};

export default Card;