import React from 'react';
import './App.scss';
import Header from './Components/Header/Header';
import Nav from './Components/Navigator/Nav';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  RouteComponentProps,
  withRouter 
} from "react-router-dom";
import Dashboard from './Components/Pages/Dashboard/Dashboard';
import Tables from './Components/Pages/Tables/Tables';
import Nomatch from './Components/Pages/Nomatch/Nomatch';
import Favorites from './Components/Pages/Products/Favorites';
import Products from './Components/Pages/Products/Products';

import ProductsProvider from './Contexts/products-context';

interface IProps {
  
}

interface IState {
  navExpand: boolean;
}

const themes = {
  light: {
    foreground: "#000000",
    background: "#eeeeee"
  },
  dark: {
    foreground: "#ffffff",
    background: "#222222"
  }
};

export const ThemeContext = React.createContext(themes.light);
class App extends React.Component<IProps, IState> {
  
  constructor(props: IProps) {
    super(props);

    this.state = {
      navExpand: true
    };

    if (window.innerWidth > 640) {
      this.setState({navExpand: true});
    } else {
      this.setState({navExpand: false});
    }
  }
  
  clickMenuHandler = () => {
    this.setState((previousState, props) => ({
      navExpand: !previousState.navExpand,
    }));
    console.log('clickMenuHandler', this.state);
  }
  
  handleItemClick = async (evt: any, id: string) => {
    console.log(evt, id);
    // this.props.history.push('/' + id.toLowerCase());
  }

  render() {
    return (
      <ProductsProvider><Router><ThemeContext.Provider value={themes.dark}>
      <div className="wrapper">
        <Nav navExpand={this.state.navExpand} handleItemClick={this.handleItemClick} />
        <div className="body">
          <Header handleMenuClick={() => this.clickMenuHandler()} navExpand={this.state.navExpand} />
          <main className="main-component">
          <Switch>
            <Route path="/tables" component={Tables} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/pages/favorites" component={Favorites} />
            <Route path="/pages/products" component={Products} />
            <Route  exact path="/" component={Dashboard} />
            <Route  exact path="*" component={Nomatch} />
          </Switch>
          </main>
        </div>
      </div>  
      </ThemeContext.Provider></Router></ProductsProvider> 
    )
  }
}

export default App;
