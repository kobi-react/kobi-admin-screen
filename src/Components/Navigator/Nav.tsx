import styles from './Nav.module.scss'; // Import css modules stylesheet as styles
import EmojiEmotions from '@material-ui/icons/EmojiEmotions';
import Dashboard from '@material-ui/icons/Dashboard';
import React from 'react';
import NavItem from './NavigatorItem/NavItem';
import Pages from '@material-ui/icons/Pages';
import TrendingUp from '@material-ui/icons/TrendingUp';
import TableChart from '@material-ui/icons/TableChart';

const Nav = (props: {navExpand: boolean, handleItemClick: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>, id: string) => void)}) => {
    const subNavItem = [
        {description: 'Full Product List', url: 'pages/products'},
        {description: 'My Favorate Products', url: 'pages/favorites'},
    ]

    // console.log(subNavItem)
    
    return (<nav className={[
                            styles.sideMenu,
                            !props.navExpand ? styles.isNorrow : ''
                            ].join(' ')}>
                <header><EmojiEmotions className={[
                            !props.navExpand ? styles.isNorrow : ''
                            ].join(' ')} /></header>
                <ol className={styles.menuItems}>
                    {/* <hr /> */}
                    <li className={styles.headerItem}>
                        <NavItem 
                            icon={<Dashboard />} 
                            description={'Dashboard'} 
                            handleItemClick={props.handleItemClick}
                            navExpand={props.navExpand}></NavItem>
                    </li>
                    {/* <hr /> */}
                    <li className={styles.headerItem2}>
                        <NavItem 
                            icon={<Pages />} 
                            description={'Pages'} 
                            handleItemClick={props.handleItemClick}
                            navExpand={props.navExpand} subNavItem={subNavItem}></NavItem></li>
                    <li className={styles.headerIte2m}>    
                        <NavItem 
                            icon={<TrendingUp />} 
                            description={'Charts'} 
                            handleItemClick={props.handleItemClick}
                            navExpand={props.navExpand}></NavItem></li>
                    <li className={styles.headerItem2}>
                        <NavItem 
                            icon={<TableChart />} 
                            description={'Tables'} 
                            handleItemClick={props.handleItemClick}
                            navExpand={props.navExpand}></NavItem></li>
                    
                </ol>
                
            </nav>)
};

export default Nav;