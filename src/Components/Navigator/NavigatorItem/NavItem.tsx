import styles from './NavItem.module.scss';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

interface SubNavItem {
    description: string,
    url: string
}

interface MyProps {
    icon: any,
    description: string,
    handleItemClick: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>, id: string) => void),
    navExpand: boolean,
    subNavItem?: Array<SubNavItem>
};

const NavItem = (props: MyProps) => {
    const [expand, setExpand] = useState(false);

    return (<><div className={[styles.wrapper, !props.navExpand ? styles.isNorrowWrapper : ''].join(' ')} onClick={(event) => {
            setExpand(!expand);
            props.handleItemClick(event, props.description);
        }}>
        <Link to={'/' + props.description.toLowerCase()}><div className={[
                styles.content,
                !props.navExpand ? styles.isNorrow : ''
            ].join(' ')}>
            <div className={styles.icon}>{props.icon}</div>
            <div className={styles.description}>{props.description}</div>
        </div>
        {props.subNavItem ? 
        <div className={[
                styles.arrow,
                expand ? styles.isExpand : '',
                !props.navExpand ? styles.isNarrowArrow : ''
        ].join(' ')}>
            <KeyboardArrowRight />
        </div> : undefined}</Link>
    </div>
        {props.subNavItem ?
            (<div className={[
                styles.subContent,
                expand ? styles.isExpand : '',
                !props.navExpand ? styles.isNarrow : ''
            ].join(' ')}>
                {props.subNavItem.map((el, index) => 
                    <Link to={'/' + el.url.toLowerCase()} key={'i' + index.toString()}><div onClick={(event) => props.handleItemClick(event, el.url)}>{el.description}</div></Link>
                )}
            </div>)
        : null}
    </>);
}

export default NavItem;