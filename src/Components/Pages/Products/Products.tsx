import React, { useContext } from 'react';

import ProductItem from '../../ProductsList/ProducItem';
import { ProductsContext } from '../../../Contexts/products-context';
import styles from './Products.module.scss';
import { useStore } from '../../../hook-store/store';

const Products = (props: any) => {
  // const productList = useContext(ProductsContext).products;
  const state: any = useStore()[0];
  // return (
  //   <ul className={styles.productslist}>
  //     {productList.map((prod: any) => (
  //       <ProductItem
  //         key={prod.id}
  //         id={prod.id}
  //         title={prod.title}
  //         description={prod.description}
  //         isFav={prod.isFavorite}
  //       />
  //     ))}
  //   </ul>
  // );
  return (
    <ul className="products-list">
      {state.products.map((prod: any) => (
        <ProductItem
          key={prod.id}
          id={prod.id}
          title={prod.title}
          description={prod.description}
          isFav={prod.isFavorite}
        />
      ))}
    </ul>
  );
};

export default Products;
