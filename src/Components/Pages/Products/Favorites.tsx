import React, { useContext } from 'react';

import FavoriteItem from '../../ProductsList/FavoriteItem';
import { ProductsContext } from '../../../Contexts/products-context';
import styles from './Products.module.scss';
import { useStore } from '../../../hook-store/store';

const Favorites = (props: any) => {
  // const favoriteProducts = useContext(ProductsContext).products.filter((p: any) => p.isFavorite);
  // let content = <p className={styles.placeholder}>Got no favorites yet!</p>;
  // if (favoriteProducts.length > 0) {
  //   content = (
  //     <ul className={styles.productslist}>
  //       {favoriteProducts.map((prod: any) => (
  //         <FavoriteItem
  //           key={prod.id}
  //           id={prod.id}
  //           title={prod.title}
  //           description={prod.description}
  //         />
  //       ))}
  //     </ul>
  //   );
  // }
  const state: any = useStore()[0];
  const favoriteProducts = state.products.filter((p: any) => p.isFavorite);
  let content = <p className="placeholder">Got no favorites yet!</p>;
  if (favoriteProducts.length > 0) {
    content = (
      <ul className="products-list">
        {favoriteProducts.map((prod: any) => (
          <FavoriteItem
            key={prod.id}
            id={prod.id}
            title={prod.title}
            description={prod.description}
          />
        ))}
      </ul>
    );
  }


  return content;
};

export default Favorites;
