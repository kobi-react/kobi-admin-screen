import { Paper } from '@material-ui/core';
import { useContext } from 'react';
import { ThemeContext } from '../../../../App';
import styles from '../Dashboard.module.scss'; 
import { useSampleCard } from './useSampleCard';

const MyCustomHook = () => {
    const data = useSampleCard();

    return (<Paper className={[styles.paper].join(' ')}>
        My Custom hook: {JSON.stringify(data)}
    </Paper>);
};

export default MyCustomHook;