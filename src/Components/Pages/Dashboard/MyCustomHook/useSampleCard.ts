import { useEffect, useState } from "react";
import { of } from "rxjs";

export function useSampleCard() {
    const [data, setData] = useState(0);

    useEffect(() => {
        fetch('https://jsonplaceholder.typicode.com/todos/1')
        .then(response => response.json())
        .then(json => {
            setData(json);
        })

        return () => {
            
        }
    }, []);

    return data;
}