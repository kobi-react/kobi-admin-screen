import { Subscribers } from "./Subscribers";

const SubscribersList = (props: {list: Array<Subscribers>}) => {
    return (<ul>
        {props.list.map((el, index) => {
            return (<li key={index}>{el.firstName} {el.lastName}</li>)
        })}
    </ul>);
};

export default SubscribersList;