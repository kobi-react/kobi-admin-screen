import { useState } from "react";
import { Subscribers } from "./Subscribers";

// demonstrate call signature
const SubscribersForm = (props: {onAddPerson: (params: Subscribers) => void}) => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const submitHandler = (event: ((event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void)) => {
        props.onAddPerson({ firstName: firstName, lastName: lastName });
    };
    
    return (<div>
        <input type="text" id="lblFirstName" placeholder="First name" value={firstName} onChange={evt => setFirstName(evt.target.value)} />
        &nbsp;
        <input type="text" id="lblLastName" placeholder="last name" value={lastName} onChange={evt => setLastName(evt.target.value)} />
        &nbsp;
        <button onClick={(event: any) => submitHandler(event)}>Add New Subscriber</button>
    </div>);
};

export default SubscribersForm;