import { Paper } from "@material-ui/core";
import { useState } from "react";
import styles from '../Dashboard.module.scss'; 
import { Subscribers } from "./Subscribers";
import SubscribersForm from "./SubscribersForm";
import SubscribersList from "./SubscribersList";

const UseState2Sample = () => {
    const [subscribers, setSubscribers] = useState([]);

    const addPerson = (person: Subscribers) => {
        setSubscribers(prevSubscribers => {
            return [
            ...prevSubscribers,
            person as never
        ]});
    };
    
    return (<Paper className={[styles.paper].join(' ')}>
        <div>
            <SubscribersForm onAddPerson={(person: Subscribers) => addPerson(person)} /><br />
            <SubscribersList list={subscribers} />
        </div>
        
    </Paper>);
};

export default UseState2Sample;