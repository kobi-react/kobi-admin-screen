import { Paper } from '@material-ui/core';
import { useEffect, useState } from 'react';
import styles from '../Dashboard.module.scss'; 

const Async = () => {
  const [posts, setPosts] = useState([]);
  const rec = 0;

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((response) => response.json())
      .then((data) => {
        setPosts(data);
      });
  }, []);

  return (
    <Paper className={[styles.paper].join(' ')}>
      <ul>
        {posts.slice(0, 5).map((post: {id: number, title: string}) => (
          <li key={post.id}>{post.title}</li>
        ))}
      </ul>
    </Paper>
  );
};

export default Async;