import { render, screen } from '@testing-library/react';
import Async from './AsyncHttpCall';

describe('Async Http call Component', () => {
    test('renders posts', async () => {
        render(<Async/>);

        const listItemElements = await screen.findAllByRole('listitem');
        expect(listItemElements).not.toHaveLength(0);
    });

    test('renders mock posts', async () => {
        window.fetch = jest.fn();
        window.fetch.mockResolvedValueOnce({
            json: async() => [{id: 'p1', title: 'first post'}]
        })
        render(<Async/>);

        const listItemElements = await screen.findAllByRole('listitem');
        expect(listItemElements).not.toHaveLength(0);
    });
});