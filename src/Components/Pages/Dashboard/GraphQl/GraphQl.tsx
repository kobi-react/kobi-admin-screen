import { gql, useQuery } from "@apollo/client";
import { Paper } from "@material-ui/core";
import { useEffect, useState } from "react";
import styles from '../Dashboard.module.scss'; 

const GraphQl = () => {
    const EXCHANGE_RATES = gql`{
        getUniqueValues(fieldName: "operator_nm")
    }`;
    const { loading, error, data } = useQuery(EXCHANGE_RATES);
    const [uniqueValues, setUniqueValues] = useState(['fffff']);

    useEffect(() => {
        if (data && data.getUniqueValues) {
            setUniqueValues(data.getUniqueValues);
        }
    }, [data]);

    return (<Paper className={[styles.paper].join(' ')}>
        GraphQl: 
        <select>
            {uniqueValues.map((el: string, index: number) => {
                return(<option key={index}>{el}</option>)
            })}
        </select>
    </Paper>);
};

export default GraphQl;