import { Paper } from "@material-ui/core";
import { useCallback, useEffect, useState } from "react";
import styles from '../Dashboard.module.scss'; 
import styles2 from './UseCallbackSample.module.scss'; 
import CountriesList from "./Subcomponents/CountriesList";
import { CountryObj } from "./Subcomponents/CountryObj";
import SearchCountries from "./Subcomponents/Search";

const UseCallbackSample = () => {
    const [loadedCountries, setLoadedCountries] = useState([]);

    useEffect(() => {
        console.log('RENDERING loadedCountries', loadedCountries);
    }, [loadedCountries]);
    
    const filteredCountriessHandler = useCallback((onLoadCountries: Array<CountryObj>) => {
        // console.log('onLoadCountries...', onLoadCountries);
        setLoadedCountries(onLoadCountries as any);
        // console.log('onLoadCountries after...', loadedCountries);
    }, []);

    return (<Paper className={[styles.paper, styles2.wrapper].join(' ')}>
        <SearchCountries onLoadCountries={filteredCountriessHandler} />
        <CountriesList loadedCountries={loadedCountries} />
    </Paper>);
};

export default UseCallbackSample;