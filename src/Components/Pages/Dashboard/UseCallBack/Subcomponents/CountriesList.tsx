import { useEffect, useState } from "react";
import { CountryObj } from "./CountryObj";
import styles from "./CountriesList.module.scss";

interface IProps {
    loadedCountries: Array<CountryObj>
}

const CountriesList = (props: IProps) => {
    return (<ul className={styles.wrapper}>
        {props.loadedCountries.map((el, index) => (
            <li key={index}>
                <img src={el.flag} alt="" /> - {el.name}
            </li>
        ))}
    </ul>)
};

export default CountriesList;