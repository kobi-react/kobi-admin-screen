import { useEffect, useRef, useState } from "react";
import useHttp from "../../../../../hook-store/useHttp";
import { CountryObj } from "./CountryObj";

interface IProps {
    onLoadCountries: (loadedContries: Array<CountryObj>) => void
}

const SearchCountries = (props: IProps) => {
    const [enteredFilter, setEnteredFilter] = useState('');
    const inputRef = useRef<HTMLInputElement>(null);
    const { isLoading, data, error, sendRequest, clear } = useHttp();
        
    // send request
    useEffect(() => {
        const timer = setTimeout(() => {
            if (enteredFilter === inputRef.current?.value) {
                if (enteredFilter && enteredFilter.length >= 3) {
                    sendRequest(
                        `https://restcountries.eu/rest/v2/name/${enteredFilter}`,
                        'GET'
                    );
                }
            }
        }, 500);
        return () => {
            clearTimeout(timer);
        }
    }, [enteredFilter, props.onLoadCountries, inputRef]);

    // recieve response
    useEffect(() => {
        if (!error && data) {
            const loadedContries: Array<CountryObj> = data;
            if (Array.isArray(loadedContries)) {
                props.onLoadCountries(loadedContries);
            } else {
                alert('there was an error')
            }
        } else if (error) {
            console.log('error...', error);
        }
    }, [data, error]);

    return (<div>
        <input 
            type="text" 
            placeholder="Search" 
            ref={inputRef} 
            value={enteredFilter} 
            onChange={event => setEnteredFilter(event.target.value)} />
    </div>);
};

export default SearchCountries;