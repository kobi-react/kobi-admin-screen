export interface CountryObj {
    name: string,
    nativeName: string,
    capital: string;
    flag: string;
    region: string;
    subregion: string;
}