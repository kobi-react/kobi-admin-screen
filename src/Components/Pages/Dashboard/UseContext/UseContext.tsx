import { Paper } from '@material-ui/core';
import { useContext } from 'react';
import { ThemeContext } from '../../../../App';
import styles from '../Dashboard.module.scss'; 

const UseContextSample = () => {
    // Declare a new state variable, which we'll call "count"
    const theme = useContext(ThemeContext);

    return (<Paper className={[styles.paper].join(' ')}>
        <button style={{ background: theme.background, color: theme.foreground }}>
            I am styled by theme context!
        </button>
    </Paper>);
};

export default UseContextSample;
