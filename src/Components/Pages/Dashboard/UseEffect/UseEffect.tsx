import { Paper } from '@material-ui/core';
import styles from '../Dashboard.module.scss'; 
import React, { useState, useEffect } from 'react';
import { of } from 'rxjs';

const UseEffectSample = () => {
    // Declare a new state variable, which we'll call "count"
    const [count, setCount] = useState(0);

    useEffect(() => {
        console.log('subscribing...')
        const source = of(1, 2, 3, 4, 5);
        const subscribe = source.subscribe((val: number) => console.log(val));

        return () => {
            subscribe.unsubscribe();
            console.log('unsubscribing...')
        }
    }, []);

    return (<Paper className={[styles.paper].join(' ')}>
        useEffect demonstration
    </Paper>);
};

export default UseEffectSample;
