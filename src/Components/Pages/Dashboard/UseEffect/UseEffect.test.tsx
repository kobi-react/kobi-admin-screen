import React from 'react';
import { render, screen } from '@testing-library/react';
import UseEffectSample from './UseEffect';

describe('useEffect Component', () => {
    test('useEffect compoenent test', () => {
        render(<UseEffectSample />);
        const linkElement = screen.getByText(/useEffect demonstration/i);
        expect(linkElement).toBeInTheDocument();
    });
}) 