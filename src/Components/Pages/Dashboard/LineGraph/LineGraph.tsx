import { Paper } from '@material-ui/core';
import React from 'react';
import styles from '../Dashboard.module.scss'; 
import MyChart from '../../../Utilities/Chart';

interface IProps {
    
}

interface IState {
  navExpand: boolean;
}

class LineGraph extends React.Component<IProps, IState> {
  
  
  constructor(props: IProps) {
        super(props);
    }

    componentDidMount() {
        
    }
    
    componentDidUpdate(prevProps: any, prevState: any, snapshot: any) {
        
    }

    render() {
        return (<Paper className={styles.paper}><MyChart /></Paper>);
    }
}

export default LineGraph;