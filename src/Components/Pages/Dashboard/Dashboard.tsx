import { Grid, GridSize, Paper } from '@material-ui/core';
import React from 'react';
import { maxWidthCellular } from '../../../constants';
import Async from './AsyncHttpCall/AsyncHttpCall';
import D3Chart from './D3Chart/D3Chart';
import styles from './Dashboard.module.scss'; 
import GraphQl from './GraphQl/GraphQl';
import LineGraph from './LineGraph/LineGraph';
import MyCustomHook from './MyCustomHook/MyCustomHook';
import UseCallbackSample from './UseCallBack/UseCallbakcSample';
import UseContextSample from './UseContext/UseContext';
import UseEffectSample from './UseEffect/UseEffect';
import UseStateSample from './UseState/UseState';
import UseState2Sample from './UseState2/UseState2';

interface IProps {
  
}

interface IState {
  navExpand: boolean;
}

class Dashboard extends React.Component<IProps, IState> {
    constructor(props: IProps) {
        super(props);
    }

    xs: GridSize = window.innerWidth < maxWidthCellular ? 12 : 6;

    render() {
        return (<div className={styles.root}>
            <Grid container spacing={3}>
                <Grid item xs={this.xs}>
                    <LineGraph />
                </Grid>
                <Grid item xs={this.xs}>
                    <D3Chart width={100} height={100} />
                </Grid>
                <Grid item xs={this.xs}>
                    <UseStateSample />
                </Grid>
                <Grid item xs={this.xs}>
                    <UseEffectSample />
                </Grid>
                <Grid item xs={this.xs}>
                    <UseContextSample />
                </Grid>
                <Grid item xs={this.xs}>
                    <MyCustomHook />
                </Grid>
                <Grid item xs={this.xs}>
                    <Async />
                </Grid>
                <Grid item xs={this.xs}>
                    <UseState2Sample />
                </Grid>
                <Grid item xs={this.xs}>
                    <UseCallbackSample />
                </Grid>
                <Grid item xs={this.xs}>
                    <GraphQl />
                </Grid>
            </Grid>
        </div>);
    }
}

export default Dashboard;