import { Paper } from '@material-ui/core';
import * as React from 'react';
//import { getDataSaga } from '../../../../Store/Actions/getSimpleGraph';
import styles from '../Dashboard.module.scss'; 
import graphStyles from './D3Chart.module.scss'; 
import { connect } from 'react-redux';
//import MyChart from '../../../Utilities/Chart';
import * as d3 from 'd3';
import { getD3ChartSaga } from '../../../../Store/Actions/getD3Data';
import { BarChart } from '../../../../types';

interface IProps {
    height: number,
    width: number,
    d3Graph: Array<BarChart>,
    getD3ChartSaga: any,
}

interface IState {
  
}

class D3Chart extends React.Component<IProps, IState> {
    ref!: SVGSVGElement;
    parentRef: React.RefObject<HTMLDivElement>;
    
    constructor(props: IProps) {
        super(props);
        this.parentRef = React.createRef();
    }

    componentDidMount() {
        // activate   
        console.log('parentRef', this.parentRef.current?.offsetWidth);
        this.props.getD3ChartSaga();
    }
    
    componentDidUpdate(prevProps: any, prevState: any, snapshot: any) {
        if (!this.arraysEqual(this.props.d3Graph, prevProps.d3Graph)) {
            this.buildGraph(this.props.d3Graph);
        }
    }

    private arraysEqual(a: Array<BarChart>, b: Array<BarChart>): boolean {
        if (a === b) return true;
        if (a == null || b == null) return false;
        if (a.length !== b.length) return false;
      
        // If you don't care about the order of the elements inside
        // the array, you should sort both arrays here.
        // Please note that calling sort on an array will modify that array.
        // you might want to clone your array first.
      
        for (var i = 0; i < a.length; ++i) {
          if (a[i] !== b[i]) return false;
        }
        return true;
    }

    private buildGraph(data: Array<BarChart>) {
        const width = Number(this.parentRef.current?.offsetWidth) - 100, height = Number(this.parentRef.current?.offsetHeight) - 64, margin = 1;

        const svg = d3.select(this.ref)
                    // .attr("preserveAspectRatio", "xMinYMin slice")
                    // .attr("viewBox", "0 75 1000 1250")
                    .attr("width", width + 60)
                    .attr("height", height + 60)

        svg.append("text")
                    .attr("transform", "translate(100,0)")
                    .attr("x", 50)
                    .attr("y", 50)
                    .attr("font-size", "24px")
                    .text("XYZ Foods Stock Price");

        const xScale = d3.scaleBand().range([0, width]).padding(0.4),
              yScale = d3.scaleLinear().range([height, 0]);

        const g = svg.append("g")
              .attr("transform", "translate(" + 60 + "," + 30 + ")");

        xScale.domain(data.map(d => d.year.toString()));
        yScale.domain([0, Math.max(...data.map(d => d.value))]);

        // bottom scale
        g.append("g")
         .attr("transform", "translate(0," + height + ")")
         .call(d3.axisBottom(xScale))
         .append("text")
         .attr("y", 30)
         .attr("x", width)
         .attr("text-anchor", "end")
         .attr("stroke", "black")
         .text("Year");

        // left scale
        g.append("g")
         .call(d3.axisLeft(yScale).tickFormat(function(d){
             return "$" + d;
         })
         .ticks(10))
         .append("text")
         .attr("transform", "rotate(-90)")
         .attr("y", 6)
         .attr("dy", "-5.1em")
         .attr("text-anchor", "end")
         .attr("stroke", "black")
         .text("Stock Price");

        // bar itself
        g.selectAll(".bar")
         .data(data)
         .enter().append("rect")
         .attr("class", "bar")
         .attr("x", d => Number(xScale(d.year.toString())))
         .attr("y", d => yScale(d.value))
         .attr("width", xScale.bandwidth())
         .attr("height", function(d) { return height - yScale(d.value); });
    }

    render() {
        return (<Paper className={[styles.paper].join(' ')} ref={this.parentRef}>
            <div className={graphStyles.svg}>
                <svg className="container" ref={(ref: SVGSVGElement) => this.ref = ref}></svg>
            </div>
        </Paper>);
    }
}

const mapStateToProps = (state: any) => {
    return {
        d3Graph: state.D3Data.d3Graph
    }
  }
  
  const mapDispatchToProps = (dispatch: any) => {
    return {
        getD3ChartSaga: (args: any) => dispatch(getD3ChartSaga(args))
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(D3Chart);