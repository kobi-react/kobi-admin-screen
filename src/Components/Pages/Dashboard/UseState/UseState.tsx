import { Paper } from '@material-ui/core';
import styles from '../Dashboard.module.scss'; 
import React, { useState, useEffect, useReducer } from 'react';

const initialState = {count: 0};

const reducer = (state: any, action: any) => {
    switch (action.type) {
      case 'increment':
        return {count: state.count + 1};
      case 'decrement':
        return {count: state.count - 1};
      default:
        throw new Error();
    }
}

const UseStateSample = () => {
    // Declare a new state variable, which we'll call "count"
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        document.title = `You clicked ${state.count} times`;
    });

    return (<Paper className={[styles.paper].join(' ')}>
        <div>You clicked {state.count} times</div>&nbsp;
        <button onClick={() => dispatch({type: 'increment'})}>
            Increase
        </button>
        <button onClick={() => dispatch({type: 'decrement'})}>
            Decrease
        </button>
    </Paper>);
};

export default UseStateSample;
