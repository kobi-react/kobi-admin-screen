import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import UseStateSample from './UseState';

describe('useState Component', () => {
    test('render initial state', () => {
        render(<UseStateSample />);

        const outputElement = screen.getByText('You clicked 0 times', { exact: false });
        expect(outputElement).toBeInTheDocument();
    });

    test('press increase button', () => {
        // arrange
        render(<UseStateSample />);

        // act
        const increaseButtonelelent = screen.getAllByText('Increase');
        userEvent.click(increaseButtonelelent[0])

        const outputElement = screen.getByText('You clicked 1 times', { exact: false });
        expect(outputElement).toBeInTheDocument();
    });

    test('press decrease button', () => {
        // arrange
        render(<UseStateSample />);

        // act
        const decreaseButtonelelent = screen.getAllByText('Decrease');
        userEvent.click(decreaseButtonelelent[0])

        const outputElement = screen.getByText('You clicked -1 times', { exact: false });
        expect(outputElement).toBeInTheDocument();
    });
});