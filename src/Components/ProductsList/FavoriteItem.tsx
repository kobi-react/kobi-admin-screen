import React from 'react';

import Card from '../../UI/card';
import './FavoriteItem.module.scss';

const FavoriteItem = (props: any) => {
  return (
    <Card style={{ marginBottom: '1rem' }}>
      <div className="favorite-item">
        <h2>{props.title}</h2>
        <p>{props.description}</p>
      </div>
    </Card>
  );
};

export default FavoriteItem;