import React, { useContext } from 'react';

import Card from '../../UI/card';
import { ProductsContext } from '../../Contexts/products-context';
import { useStore } from '../../hook-store/store';
import styles from './ProductItem.module.scss';

const ProductItem = (props: any) => {

  // const toggleFav = useContext(ProductsContext).toggleFav;
  const dispatch: any = useStore(false)[1];

  // const toggleFavHandler = () => {
  //   toggleFav(props.id);
  // };

  const toggleFavHandler = () => {
    // toggleFav(props.id);
    dispatch('TOGGLE_FAV', props.id);
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <div className={styles.productitem}>
        <h2 className={props.isFav ? styles.isfav : ''}>{props.title}</h2>
        <p>{props.description}</p>
        <button
          className={!props.isFav ? 'button-outline' : ''}
          onClick={toggleFavHandler}
        >
          {props.isFav ? 'Un-Favorite' : 'Favorite'}
        </button>
      </div>
    </Card>
  );
};

export default ProductItem;