import styles from './Header.module.scss'; // Import css modules stylesheet as styles
import Menu from '@material-ui/icons/Menu';
import React, { ReactElement } from 'react';

const Header = (props: {handleMenuClick: ((event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void), navExpand: boolean}): ReactElement => {
    return (
        <header className={styles.navHeader}>
            <div className={[
                            styles.leftDiv,
                            !props.navExpand ? styles.isNorrow : ''
                            ].join(' ')} onClick={props.handleMenuClick}><Menu /></div>
            <div>asdadaads</div>
        </header>);
};

export default Header;