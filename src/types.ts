export interface InitialState { 
    d3Graph: Array<number>
};

export interface BarChart {
    year: number,
    value: number
}