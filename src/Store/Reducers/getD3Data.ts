import { D3CHART_DATA_LOADED } from "../Action-Types";
import initialState from "../state";

function D3DataReducer(state = initialState, action: any) {
    if (action.type === D3CHART_DATA_LOADED) {
        return Object.assign({}, state, {
            d3Graph: state.d3Graph = action.payload
        });
    }

    return state;
}

export default D3DataReducer;