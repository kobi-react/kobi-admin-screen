import { takeEvery, call, put } from "redux-saga/effects";
import { BarChart } from "../../types";
import { API_ERRORED, GET_D3CHART_DATA, D3CHART_DATA_LOADED } from "../Action-Types";

export default function* watcherSaga() {
    yield takeEvery(GET_D3CHART_DATA, workerSaga);
}

/**
 * See {@link MyClass} and [MyClass's foo property]{@link MyClass#foo}.
 * Also, check out {@link http://www.google.com|Google} and
 * {@link https://github.com GitHub}.
 * @param {*} args 
 */
function* workerSaga(args: any): any {
    try {
        const payload = yield call(getDataSaga, args);
        yield put({ type: D3CHART_DATA_LOADED, payload });
    } catch (e) {
        yield put({ type: API_ERRORED, payload: e });
    }
}

function getDataSaga(args: any): Promise<Array<BarChart>> {
    return fetch("/JSON/barchart.json").then(response =>
        response.json()
    );
}