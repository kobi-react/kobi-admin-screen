import { InitialState } from '../types';

const initialState: InitialState = {
    d3Graph: []
};

export default initialState;