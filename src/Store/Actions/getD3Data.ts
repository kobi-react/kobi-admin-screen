import { GET_D3CHART_DATA } from "../Action-Types";

export function getD3ChartSaga(args: Array<number>) {
    return { type: GET_D3CHART_DATA, args };
}